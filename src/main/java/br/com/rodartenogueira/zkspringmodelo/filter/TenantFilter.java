package br.com.rodartenogueira.zkspringmodelo.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;

import br.com.rodartenogueira.zkspringmodelo.TenantContext;
import br.com.rodartenogueira.zkspringmodelo.request.MyServletRequestWrapper;

@Component
public class TenantFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		MyServletRequestWrapper httpReq = new MyServletRequestWrapper((HttpServletRequest) request);
		HttpServletResponse httpRes = (HttpServletResponse) response;
		HttpSession session = httpReq.getSession();

		if (httpReq.getHeader("BANCO") == null) {
			TenantContext.setCurrentTenant("0020");//TODO TESTE setar 20 se nao tiver o header definido
			System.out.println("Volta pro gerenciador e notifica erro");

		}else {
			TenantContext.setCurrentTenant(httpReq.getHeader("BANCO"));
		}

		chain.doFilter(httpReq, httpRes);

	}
	public void init(FilterConfig config) throws ServletException {

		System.out.println("init() method has been get invoked");
		System.out.println("Filter name is " + config.getFilterName());
		System.out.println("ServletContext name is" + config.getServletContext());
		System.out.println("init() method is ended");
	}


	public void destroy() {
		// do some stuff like clearing the resources

	}
}
