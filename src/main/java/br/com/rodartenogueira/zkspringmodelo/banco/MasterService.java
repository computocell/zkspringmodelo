package br.com.rodartenogueira.zkspringmodelo.banco;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import lombok.Builder;

public class MasterService {


	public static Map<Object, Object> getDataSourceHashMap() {
		
//		DriverManagerDataSource dataSource_0020 = new DriverManagerDataSource();
//		dataSource_0020.setDriverClassName(DATASOURCE_DRIVER_CLASS_NAME);
//		dataSource_0020.setUrl("jdbc:mysql://localhost:3306/0020");
//		dataSource_0020.setUsername(DATASOURCE_USER_NAME);
//		dataSource_0020.setPassword(DATASOURCE_USER_PASSWORD);
//
//		DriverManagerDataSource dataSource = new DriverManagerDataSource();
//		dataSource.setDriverClassName(DATASOURCE_DRIVER_CLASS_NAME);
//		dataSource.setUrl("jdbc:mysql://localhost:3306/"+TenantContext.getCurrentTenant());
//		dataSource.setUsername(DATASOURCE_USER_NAME);
//		dataSource.setPassword(DATASOURCE_USER_PASSWORD);

		HashMap<Object, Object> mpDataSources = new LinkedHashMap<>();
		
		
		
//		hashMap.put( (TenantContext.getCurrentTenant() !=null  ? TenantContext.getCurrentTenant() : "0020") ,  (TenantContext.getCurrentTenant() != null ? dataSource : dataSource_0020) );
		
		
		for(int i = 01;i<21; i++) {
			String numeroFormatoBanco = "00"+String.format("%02d", i);
			mpDataSources.put(numeroFormatoBanco	,new BuilderDriverManagerDataSource().builder().banco(numeroFormatoBanco).build());
		}

		return mpDataSources;
	}
	
	
	
}
class BuilderDriverManagerDataSource {
	private final static String DATASOURCE_USER_NAME = "system";
	private final static String DATASOURCE_USER_PASSWORD = "systemlog";
	private final static String DATASOURCE_DRIVER_CLASS_NAME = "com.mysql.jdbc.Driver";
	
	DriverManagerDataSource dataSource;
	
	@Builder
	public DriverManagerDataSource gerarSource(String banco) {
		dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(DATASOURCE_DRIVER_CLASS_NAME);
		dataSource.setUrl("jdbc:mysql://localhost:3306/"+banco);
		dataSource.setUsername(DATASOURCE_USER_NAME);
		dataSource.setPassword(DATASOURCE_USER_PASSWORD);
		return dataSource;
	}
}
